# sphinx-immaterial-igwn

This project defines the IGWN Material theme for Sphinx, an extension of
[Sphinx Immaterial](https://jbms.github.io/sphinx-immaterial/).

To install this package:

```shell
python -m pip install sphinx-immaterial-igwn
```

To use the theme add the following to your `conf.py`:

```python
extensions = ["sphinx_immaterial_igwn"]
html_theme = "sphinx_immaterial_igwn"
```

For full details, see <https://computing.docs.ligo.org/igwn-material/sphinx/>.
