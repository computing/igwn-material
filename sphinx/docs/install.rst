############
Installation
############

Sphinx-Immaterial-IGWN can be installed from
`conda-forge <https://conda-forge.org>`__:

.. code-block:: shell

   conda install --channel conda-forge sphinx-immaterial-igwn

Alternatively, it can be installed using
`Pip <https://pip.pypa.io>`__:

.. code-block:: shell

   python -m pip install sphinx-immaterial-igwn
