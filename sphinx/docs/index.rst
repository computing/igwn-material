######################
Sphinx-Immaterial-IGWN
######################

.. toctree::
   :hidden:

   Home <self>
   Install <install>
   Usage <usage>

The ``sphinx-immaterial-igwn`` project provides
`IGWN <https://www.ligo.org>`__ extensions to
`Sphinx-Immaterial <https://jbms.github.io/sphinx-immaterial/>`__.

This theme alters the look-and-feel of the upstream
``material`` theme for MkDocs to provide a common baseline for
IGWN sites.
