# IGWN Material for MkDocs

This project defines the IGWN Material theme for MkDocs, an extension of
[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).

To install this package:

```shell
python -m pip install mkdocs-material-igwn
```

To use the theme add the following to your `mkdocs.yml`:

```yaml
theme:
  name: material_igwn
```

For full details, see <https://computing.docs.ligo.org/igwn-material/mkdocs/>.
