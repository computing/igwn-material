---
title: Home
author: Duncan Macleod <duncan.macleod@ligo.org>
---

# MkDocs Material for IGWN

The `mkdocs-material-igwn` project provides
[IGWN](https://www.ligo.org) extensions to
[MkDocs Material](https://squidfunk.github.io/mkdocs-material/).

This theme alters the look-and-feel of the upstream
`material` theme for MkDocs to provide a common baseline for
IGWN sites.
